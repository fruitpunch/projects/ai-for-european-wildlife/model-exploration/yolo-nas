from super_gradients.training.dataloaders.dataloaders import coco_detection_yolo_format_val
from super_gradients.training import Trainer
from super_gradients.training import models
from super_gradients.training.losses import PPYoloELoss
from super_gradients.training.metrics import DetectionMetrics_050_095, DetectionMetrics_050
from super_gradients.training.models.detection_models.pp_yolo_e import PPYoloEPostPredictionCallback
from torchinfo import summary
import argparse
import yaml
import time
import torch
import os

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-d", "--data", type=str, required=True,
                    help="Path to data.yaml")
    ap.add_argument("-o", "--output-dir", type=str,
                    help="Checkpoint dir name")
    ap.add_argument("-m", "--model", type=str, required=True,
                    help="Model type (eg: yolo_nas_s)")
    ap.add_argument("-w", "--weight", type=str,
                    help="Path to trained model weights to use for testing")
    ap.add_argument("-c", "--conf", type=str, default=None, help="Path to test configuration yaml file. If None, default hyperparameters will be used.")
    
    ap.add_argument("-s", "--summary", action='store_true', help="Print model summary")

    ap.add_argument("-b", "--batch", type=int, default=8,
                    help="Testing batch size"),
    ap.add_argument("-j", "--worker", type=int, default=2,
                    help="Testing number of workers")
    ap.add_argument("-z", "--img-width", type=int, default=640, help="Width of test images")

    ap.add_argument("-e", "--img-height", type=int, default=640, help="Height of test images")

    args = vars(ap.parse_args())

    s_time = time.time()


    if args['output_dir'] is None:
        out_dir = 'output'
    else:
        out_dir = args['output_dir']

    device=torch.device("cuda" if torch.cuda.is_available() else "cpu")

    trainer = Trainer(experiment_name=out_dir, ckpt_root_dir='runs')
    
    yaml_params = yaml.safe_load(open(args['data'], 'r'))

    if 'test' in (yaml_params['images'].keys() and yaml_params['labels'].keys()):
        test_data = coco_detection_yolo_format_val(
            dataset_params={
                'data_dir': yaml_params['path'],
                'images_dir': yaml_params['images']['test'],
                'labels_dir': yaml_params['labels']['test'],
                'classes': yaml_params['names']
            },
            dataloader_params={
                'batch_size': args['batch'],
                'num_workers': args['worker']
            }
        )
    else:  
        print("[ERROR] No test data found in data.yaml")
        exit()

    # Evaluating on Test Dataset
    model = models.get(args['model'], num_classes=len(yaml_params['names']), checkpoint_path=os.path.join('runs', args['weight']))

    model = model.to(device)

    if args['summary']:
        sum=summary(model=model, 
        input_size=(args["batch"], 3, args["img_width"], args["img_height"]),
        col_names=["input_size", "output_size", "num_params", "trainable"],
        col_width=20,
        row_settings=["var_names"])
        
        print(sum)

    if args['conf'] is not None:
        #WIP
        test_params=yaml.safe_load(open(args['conf'], 'r'))
        print("[INFO] Test metrics: ", trainer.test(model= model,
                test_loader=test_data,
                test_metrics_list=test_params['test_metrics_list']))
    else:
        print("[INFO] Test metrics: ", trainer.test(model= model,
                    test_loader=test_data,
                    test_metrics_list=DetectionMetrics_050_095(score_thres=0.1,
                                                        top_k_predictions=300,
                                                        num_cls=len(yaml_params['names']),
                                                        normalize_targets=True,
                                                        post_prediction_callback=PPYoloEPostPredictionCallback(
                                                            score_threshold=0.01,
                                                            nms_top_k=1000,
                                                            max_predictions=300,
                                                            nms_threshold=0.7)
                                                        )))

    print(f'[INFO] Test Completed in {(time.time() - s_time) / 60} minutes')
