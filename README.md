# YOLO-NAS

Repository containing code for training, testing and inference using the new [YOLO-NAS model](https://github.com/Deci-AI/super-gradients/blob/master/YOLONAS.md). 

## Overview

This repository contains basically three python scripts: 

  - `train.py` - for training the *YOLO-NAS* model
  - `test.py` - for testing the trained model
  - `inference.py` - for inference using the trained model

Additionally, a python notebook `Train_YOLONAS_scripts.ipynb` is provided with an example of how to use the scripts in Google Colab.

The optional `train_conf.yaml` file contains the configuration parameters for the training process and the `test_conf.yaml` file contains the parameters for testing. 

## Installation

### Download this repository 

Clone the repository using:

` git clone  https://gitlab.com/fruitpunch/projects/ai-for-european-wildlife/model-exploration/yolo-nas.git`

### Create a new python environment

An environment with Python 3.10.6 is recommended, as this code was developed using it, but any version of Python 3.7+ will work too. 
Then activate the environment and install the requirements using the command:

`pip install -r requirements.txt `

## Usage 

All the three scripts mentioned above can be used directly from the command line with different arguments. 

### Training

The `train.py` script takes the following required arguments: 

- `-d` or `--data` - path to the `data.yaml` file containing some information about the dataset. An example of the format of this file can be found in the `data.yaml` file in this repository.
- `-m` or `--model` - name of the model to be used. The available models are: `yolo_nas_s`, `yolo_nas_m`, `yolo_nas_l`.

And the following optional arguments:

- `-o` or `--output-dir` - name of the directory where the checkpoints will be saved. By default it is `output`. An index will be added to the end of the name to avoid overwriting previous checkpoints.

- `-w` or `--weight` - path to the pre-trained weights. By default it is `coco`.
- `-c` or `--conf` - path to the `train_conf.yaml` file containing the training configuration parameters. If not provided, the default parameters will be used.
- `-b` or `--batch` - training batch size. By default it is 8.
- `-j` or `--worker` - number of workers for the training process. By default it is 2.

Example of usage:

``` bash
python train.py -d data.yaml -m yolo_nas_s -o output -b 16 -j 4
```
### Testing

The `test.py` script requires the same arguments as `train.py`, with the addition of the `-w` or `--weight` argument, which is required to specify the path to the trained weights to be used for testing.

As for the optional arguments, they are the same received by `train.py`, but an additional `-s` or `--summary` argument is added, which is used to indicate that the model summary should be printed.

Example of usage:

``` bash
python test.py -d data.yaml -m yolo_nas_s -w output0/average_model.pth -o output -b 8 -s
```

### Inference

The necessary arguments for the `inference.py` script are the same as for `test.py`.

Regarding the optional arguments, they are the following:

- `--confidence` - model prediction confidence. It must be a value between 0 and 1. By default it is 0.25.
- `--save` - to save the predictions made by the model on the images. By default it is False.
- `-o` or `--output-dir` - name of the directory where the predictions on the images will be saved. By default it is `detect`.
- `--show` - to show the predictions on the images. By default it is False.
- `--sahi` - to use the [SAHI](https://github.com/obss/sahi) method to make the predictions. By default it is False.
- `--sahi-classes` - path to a yaml file containing the list of classes in the dataset. This argument is required if `--sahi` is True.

Example of usage:

``` bash
python inference.py -i data.yaml -m yolo_nas_s -w output0/average_model.pth -o output0/detect --confidence 0.3 --save --show
```

In any case, for more information about the arguments, you can use the `-h` or `--help` argument to print the help message.



















































