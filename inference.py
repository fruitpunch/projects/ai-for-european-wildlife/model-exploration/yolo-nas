from super_gradients.training import models
import torch
import cv2
import random
import numpy as np
import time
import argparse
import yaml
import os
import supervision as sv
import glob

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--data", type=str, required=True,
                    help="path to data.yaml")
    ap.add_argument("-m", "--model", type=str, required=True,
                    help="Model type (eg: yolo_nas_s)")
    ap.add_argument("-o", "--output-dir", type=str, default='detect',
                    help="Output dir name")
    ap.add_argument("-w", "--weight", type=str, required=True,
                    help="path to trained model weight")
    ap.add_argument("--confidence", type=float, default=0.25,
                    help="model prediction confidence (0<conf<1)")
    ap.add_argument("--save", action='store_true',
                    help="Save predictions on images.")
    ap.add_argument("--show", action='store_true', help="Show predictions on images.")
    
    ap.add_argument("--sahi", action='store_true', help="Use SAHI for the predictions.")
    ap.add_argument("--sahi-classes", type=str, default=None, help="Path to yaml file with a list of the clases in the dataset needed for SAHI.")


    args = vars(ap.parse_args())
    
    s_time = time.time()
    
    yaml_params = yaml.safe_load(open(args['data'], 'r'))

    if 'inference' in yaml_params['images'].keys():

        device=torch.device("cuda" if torch.cuda.is_available() else "cpu")

        if args['sahi']:

            from sahi.predict import predict

            model_type = "yolonas"
            model_name=args['model']
            model_path = os.path.join('runs', args['weight'])
            model_device = device
            model_confidence_threshold = 0.4

            slice_height = 256
            slice_width = 256
            overlap_height_ratio = 0.2
            overlap_width_ratio = 0.2

            source_image_dir = os.path.join(yaml_params['path'], yaml_params['images']['inference'])

            predict(
                class_names_yaml_path=args['sahi_classes'],
                model_type=model_type,
                model_name=model_name,
                model_path=model_path,
                model_device=model_device,
                model_confidence_threshold=model_confidence_threshold,
                source=source_image_dir,
                slice_height=slice_height,
                slice_width=slice_width,
                overlap_height_ratio=overlap_height_ratio,
                overlap_width_ratio=overlap_width_ratio,
                project='runs',
                name=args['output_dir'],
            )
        else:

            # Load YOLO-NAS Model
            model = models.get(
                args['model'],
                num_classes=len(yaml_params['names']),
                checkpoint_path=os.path.join('runs', args['weight'])
            )
            
            model = model.to(device)

            for img_name in list(glob.glob((os.path.join(yaml_params['path'], yaml_params['images']['inference'], '*.jpg')))):
                print("[INFO] Reading image ", img_name)
                img=cv2.imread(img_name)
                result = list(model.predict(img, conf=args['confidence']))[0]

                detections = sv.Detections(
                    xyxy=result.prediction.bboxes_xyxy,
                    confidence=result.prediction.confidence,
                    class_id=result.prediction.labels.astype(int)
                )

                box_annotator = sv.BoxAnnotator()

                labels = [
                    f"{result.class_names[class_id]} {confidence:0.2f}"
                    for _, _, confidence, class_id, _
                    in detections
                ]

                annotated_frame = box_annotator.annotate(
                    scene=img.copy(),
                    detections=detections,
                    labels=labels
                )

                if args["show"]:
                    cv2.namedWindow("Annotated Frame", cv2.WINDOW_KEEPRATIO)
                    cv2.imshow("Annotated Frame", annotated_frame)
                    cv2.resizeWindow("Annotated Frame", 2000, 2000)

                    cv2.waitKey(0)
                    cv2.destroyAllWindows()
                else: 
                    print(f'Predictions: {labels}')
                
                if args["save"]:
                    save_path = os.path.join('runs', args['output_dir'])
                    if not os.path.exists(save_path):
                        os.makedirs(save_path)
                    cv2.imwrite( os.path.join( save_path, os.path.split(img_name)[-1]), annotated_frame)
                    print(f"[INFO] Prediction saved in {save_path}")  

            print(f'[INFO] Inference Completed in {(time.time() - s_time) / 60} minutes')
    else:
        print("[ERROR] No inference data found in {}".format(args['data']))
        exit()